
import java.io.InputStream;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.stage.Stage;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

public class App extends Application {
    public static void main(String[] args) throws Exception {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        
        VBox containerLeft = createContainterLeft();

        VBox containerRight = createContainerRight();
        
        HBox root = new HBox();
        root.getChildren().addAll(containerLeft, containerRight);
        HBox.setHgrow(containerLeft, Priority.ALWAYS); // Cada vez que cambien las dimensiones de la ventana, los componentes se deben redimensionar
        HBox.setHgrow(containerRight, Priority.ALWAYS); // Cada vez que cambien las dimensiones de la ventana, los componentes se deben redimensionar

        Scene scene = new Scene(root, 844, 503);
        
        primaryStage.setScene(scene);
        primaryStage.setTitle("Login with HBox in JavaFX");
        primaryStage.show();

    }

    private VBox createContainterLeft(){
        VBox containerLeft = new VBox();
        
        Label labelTitle = new Label("Login");
        labelTitle.setFont(new Font(40)); // sirve para cambiar la fuente del label

        Label labelUser = new Label("User");
        labelUser.setFont(new Font(20));
        
        Label labelPassword = new Label("Password");
        labelPassword.setFont(new Font(20));
        
        TextField textUser = new TextField();
        textUser.setFont(new Font(18));
        textUser.setPromptText("Enter a user"); // holdplace
        textUser.setPrefWidth(341); // pre define una longitud por defecto
        textUser.setPrefHeight(44); // pre define una longitud por defecto
        
        PasswordField textPassword = new PasswordField();
        textPassword.setFont(new Font(18));
        textPassword.setPromptText("Enter your password"); // holdplace
        textPassword.setPrefWidth(341); // pre define una longitud por defecto
        textPassword.setPrefHeight(44); // pre define una longitud por defecto
        
        Button buttonLogin = new Button("Login");
        buttonLogin.setFont(new Font(20));
        buttonLogin.setPrefWidth(341); // pre define una longitud por defecto
        buttonLogin.setPrefHeight(44); // pre define una longitud por defecto
        buttonLogin.setMaxWidth(Double.MAX_VALUE);
        buttonLogin.setCursor(Cursor.HAND);

        VBox containerLogin = new VBox();
        containerLogin.getChildren().addAll(labelUser, textUser, labelPassword, textPassword, buttonLogin);
        containerLogin.setAlignment(Pos.TOP_LEFT); // Posicionar el texto
        
        VBox.setMargin(labelUser, new Insets(10, 0, 0, 0)); // aplicamos margenes a un Nodo en (top, right, button, left)
        VBox.setMargin(labelPassword, new Insets(10, 0, 0, 0));
        VBox.setMargin(buttonLogin, new Insets(20, 0, 0, 0));

        containerLeft.getChildren().addAll(labelTitle, containerLogin);
        containerLeft.setPrefWidth(422);
        containerLeft.setAlignment(Pos.CENTER);
        VBox.setMargin(containerLogin, new Insets(0, 30, 0, 30));

        return containerLeft;
    }

    private VBox createContainerRight(){
        VBox containerRight = new VBox();

        InputStream inputStream = getClass().getResourceAsStream("img/login.png");
        Image image = new Image(inputStream);

        ImageView imageLogo = new ImageView(image);
        imageLogo.setFitHeight(450);
        imageLogo.setFitWidth(450);
        containerRight.getChildren().add(imageLogo);

        containerRight.setPrefWidth(422);
        containerRight.setAlignment(Pos.CENTER);
        containerRight.setBackground(new Background( new BackgroundFill( Color.web("#30373e"), CornerRadii.EMPTY, Insets.EMPTY)));
        return containerRight;
    }

}
